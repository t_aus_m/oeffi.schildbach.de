# version alert
alert-version-android       | max-sdk:15                            | 1d | warning
alert-version-amazon        | min-sdk:16 version:653                | 1d | warning

# disruptions
alert-disruption-met		| network:MET							| 1d | warning

# hints
hint-navigation-drawer		| prefs-show-info:true limit-info:2d	| once | info
hint-network-picker			| prefs-show-info:true limit-info:2d	| once | info
hint-landscape-orientation	| prefs-show-info:true limit-info:2d	| once | info
hint-qr-and-nfc				| prefs-show-info:true limit-info:2d	| once | info
hint-free-software			| prefs-show-info:true limit-info:2d	| once | info
