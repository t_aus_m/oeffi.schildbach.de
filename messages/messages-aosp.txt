# version alert
alert-version-android       | max-sdk:15                            | 1d | warning
alert-version-google        | min-sdk:16 max-sdk:18 version:654     | 1d | warning
alert-version-google        | min-sdk:19 version:668                | 1d | warning

# disruptions
alert-disruption-met		| network:MET							| 1d | warning

# hints
hint-navigation-drawer		| prefs-show-info:true limit-info:2d	| once | info
hint-network-picker			| prefs-show-info:true limit-info:2d	| once | info
hint-landscape-orientation	| prefs-show-info:true limit-info:2d	| once | info
hint-qr-and-nfc				| prefs-show-info:true limit-info:2d	| once | info
hint-launcher-shortcuts		| prefs-show-info:true limit-info:2d	| once | info
hint-location-problems		| prefs-show-info:true limit-info:2d	| once | info
hint-drag-stations			| prefs-show-info:true limit-info:2d min-version:546 task:stations | once | info
hint-free-software			| prefs-show-info:true limit-info:2d	| once | info
